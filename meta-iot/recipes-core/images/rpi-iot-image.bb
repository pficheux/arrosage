# Base this image on core-image-minimal
include recipes-core/images/core-image-minimal.bb

IMAGE_FEATURES += "package-management ssh-server-dropbear allow-empty-password empty-root-password"
IMAGE_INSTALL += " kernel-modules wiringpi i2c-tools iw wpa-supplicant mpl115a2 mosquitto-clients"

