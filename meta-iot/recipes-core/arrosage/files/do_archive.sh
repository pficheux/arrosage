#!/bin/sh

BASE=/www/pages

D=$(date +%Y_%m_%d)
H=$(date +%H:%M:%S)

if [! -r $BASE/archives ]; then
    mkdir $BASE/archives
fi

mv ${BASE}/arrosage.html  $BASE/archives/arrosage_${D}_${H}.html

/etc/init.d/arrosage restart
