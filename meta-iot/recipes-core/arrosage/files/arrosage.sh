#!/bin/sh

PREF=/sys/class/gpio
GPIO_IN=25
GPIO_OUT=24

relay_on ()
{
    echo 0 > ${PREF}/gpio${GPIO_OUT}/value
}

relay_off ()
{
    echo 1 > ${PREF}/gpio${GPIO_OUT}/value
}

probe_value ()
{
    echo $(cat ${PREF}/gpio${GPIO_IN}/value)
}

if [ ! -r ${PREF}/gpio${GPIO_IN} ]; then
    echo ${GPIO_IN} > ${PREF}/export
    echo in > ${PREF}/gpio${GPIO_IN}/direction
fi

if [ ! -r ${PREF}/gpio${GPIO_OUT} ]; then
    echo ${GPIO_OUT} > ${PREF}/export
    echo out > ${PREF}/gpio${GPIO_OUT}/direction
    relay_off
fi

echo "<HEAD><META http-equiv=\"refresh\" content=\"10; URL=/arrosage.html\"></HEAD>"

while [ 1 ]
do
    D=$(date +%Y/%m/%d)
    H=$(date +%H:%M:%S)

    echo -n "$D $H "

    if [ $(probe_value) -eq 1 ]; then
	echo -n "<B>IL FAUT ARROSER"
	relay_on
    else
	echo -n "OK"
	relay_off
    fi
    echo " -> </B><A HREF=\"$D\">voir les images</A><BR>"

    sleep 900 
done 
