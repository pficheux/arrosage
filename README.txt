29/7/2017
=========

- Capteur TE215, utilisation sortie numéri (1/0)

- Câblage breadboard = bouton GPIO (résistance 10K) -> voir pics/

- Premier test (B+ sous Krogoth), ajout script 'arrosage.sh'

-> plantage wpa_supplicant au ifup wlan0 sous Morty !!

30/7/2017
=========

- Test Wi-Fi (meta-iot)

31/7/2017
=========

- Ajout script camera.sh exécuté par cron (crée les images dans /www/pages/2017/07/...)

0,30 15,16,17 * * * /home/root/camera.sh

- arrosage.sh produit une page HTML dyamique (un bon vieux META http-equiv="refresh"...)

- Ajout des pkgs Yocto nécessaires (voir article Yocto/IoT)

IMAGE_INSTALL += " kernel-modules iw wpa-supplicant cronie lighttpd userland ntpdate tzdata"

- config Lighttpd pour lister des répertoires

## virtual directory listings
dir-listing.activate       = "enable"

7/8/2017
========

- Ajout script 'do_archive.sh'

Modif crontab ->

0 7,9,11,13,15,17,19,21 * * * /usr/bin/camera.sh
59 23 * * * /usr/bin/do_archive.sh

9/8/2017
========

- Test Assetwolf

$ mosquitto_pub -h pficheux.assetwolfportal.com -t up/client/pficheux/asset/jdnk6w -m "{ \"device_id\" : \"jdnk6w\" , \"temperature\" : 100 }" -u pficheux -P <password>

-> OK

-> pb du "schema" qui n'apparaît pas dans la doc ???

- Modif get_temp.sh

* asset en flottant
* ajout mosquitto-clients -> meta-intel-iot-middleware

$ bitbake mosquitto
...
