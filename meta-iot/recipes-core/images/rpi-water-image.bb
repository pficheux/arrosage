# Base this image on core-image-minimal
include recipes-core/images/core-image-minimal.bb

IMAGE_FEATURES += "package-management ssh-server-dropbear allow-empty-password empty-root-password"
IMAGE_INSTALL += " kernel-modules iw wpa-supplicant cronie lighttpd userland arrosage"
