DESCRIPTION = "Water utility"
SECTION = "examples"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=8ca43cbc842c2336e835926c2166c28b"

SRC_URI = "file://COPYING file://init file://arrosage.sh file://camera.sh file://do_archive.sh"

inherit update-rc.d allarch

S = "${WORKDIR}"

INITSCRIPT_NAME = "arrosage"
INITSCRIPT_PARAMS = "defaults 99"

 
do_install() {
  	     install -d ${D}${bindir}
	     install -m 0755 arrosage.sh ${D}${bindir}
       	     install -m 0755 camera.sh ${D}${bindir}
       	     install -m 0755 do_archive.sh ${D}${bindir}
	     install -d ${D}${sysconfdir}/init.d
	     install -m 0755 ${WORKDIR}/init  ${D}${sysconfdir}/init.d/arrosage
}
