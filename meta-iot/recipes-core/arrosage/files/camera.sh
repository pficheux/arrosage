#!/bin/sh

BASE=/www/pages

W=640
H=480

PIC_DIR=$BASE/$(date +%Y/%m/%d)

if [ ! -r $PIC_DIR ]; then
    mkdir -p $PIC_DIR
fi

PIC_NAME=$(date +%H_%M_%S)

raspistill -w $W -h $H -e png -o ${PIC_DIR}/${PIC_NAME}.png
